import { sleep } from 'k6';
import http from 'k6/http';

export let options = {
  duration: '1m',
  vus: 20,
  thresholds: {
    http_req_duration: ['p(95)<500'], // 95 percent of response times must be below 500ms
  },
  ext: {
    loadimpact: {
      // Project: Default project
      projectID: 3686241,
      // Test runs with the same name groups test runs together.
      name: 'Test (12/03/2024-10:28:45)'
    }
  }  
};

export default function () {
  http.get('http://test.k6.io/contacts.php');
  sleep(3);
}